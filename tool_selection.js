

function Rectangle(x1, y1, x2, y2) {
	if (x1 > x2) {
		this.x1 = x2;
		this.x2 = x1;
	}
	else {
		this.x1 = x1;
		this.x2 = x2;
	}

	if (y1 > y2) {
		this.y1 = y2;
		this.y2 = y1;
	}
	else {
		this.y1 = y1;
		this.y2 = y2;
	}

	this.contains = function (x, y) {
		return (x1 <= x) && (x <= x2) && (y1 <= y) && (y <= y2);
	}
}










function cursorMakeRectangle() {
	$(".canvasForeground").css('cursor', 'crosshair');

}




function cursorGrabbing() {
	$(".canvasForeground").css('cursor', 'grabbing');

}

// This painting tool works like a drawing pencil which tracks the mouse
// movements.
function tool_selection() {
	var tool = this;
	this.started = false;
	this.moveSelection = true;
	this.page = undefined;
	var x1 = 0;
	var x2 = 0;

	// This is called when you start holding down the mouse button.
	// This starts the pencil drawing.
	this.mousedown = function (ev, x, y) {
		y = getSurLigneOuEntre(y);
		if (tool.selection != null && tool.selection.contains(x, y)) {
			tool.moveSelection = true;
		}
		else {
			tool.selection = null;
			tool.moveSelection = false;
		}
		tool.page = ev.page;
		tool.started = true;
		tool.x1 = x;
		tool.y1 = y;
	};


	this.getDeltaY = function (y) {
		return Math.round((y - tool.y1) / noteradius) * noteradius;

	}


	// This function is called every time you move the mouse. Obviously, it only
	// draws if the tool.started state is set to true (when you are holding down
	// the mouse button).
	this.mousemove = function (ev, x, y) {
		y = getSurLigneOuEntre(y);
		if (tool.started) {
			if (tool.moveSelection) {
				update();
				var width = tool.selection.x2 - tool.selection.x1;
				var height = tool.selection.y2 - tool.selection.y1;

				ev.page.getForegroundContext().drawImage(ev.page.getBackgroundCanvas(), tool.selection.x1, tool.selection.y1, width, height,
					tool.selection.x1 + x - tool.x1, tool.selection.y1 + tool.getDeltaY(y), width, height);

			}
			else {
				tool.selection = new Rectangle(tool.x1, tool.y1, x, y);
				update();
			}
		}
		else {
			if (tool.selection != null && tool.selection.contains(x, y)) {
				cursorGrabbing();
			}
			else {
				cursorMakeRectangle();
			}
		}
	};

	// This is called when you release the mouse button.
	this.mouseup = function (ev, x, y) {
		if (tool.started) {
			tool.mousemove(ev);

			if (tool.moveSelection) {
				var width = tool.selection.x2 - tool.selection.x1;
				var height = tool.selection.y2 - tool.selection.y1;
				ev.page.getBackgroundContext().drawImage(ev.page.getBackgroundCanvas(), tool.selection.x1, tool.selection.y1, width, height,
					tool.selection.x1 + x - tool.x1, tool.selection.y1 + tool.getDeltaY(y), width, height);

				doc.undoRedoScreenShot(ev.page);
			}
			else {
				y = getSurLigneOuEntre(y);
				tool.selection = new Rectangle(tool.x1, tool.y1, x, y);
			}


			tool.started = false;
			update();
		}
	};

	this.touchstart = this.mousedown;
	this.touchmove = this.mousemove;
	this.touchend = this.mouseup;


	this.quit = function () {

	}


	this.deleteSelection = () => {
		const selection = this.selection;
		const imagedata = getImageData(this.page.getBackgroundContext(), selection.x1, selection.y1, selection.x2 - selection.x1, selection.y2 - selection.y1);

		for (var x = selection.x1; x <= selection.x2; x++)
			for (var y = selection.y1; y <= selection.y2; y++)
				setImageDataPixel(imagedata, x, y, 0);

		this.page.getBackgroundContext().putImageData(imagedata.data, selection.x1, selection.y1);
		doc.undoRedoScreenShot(this.page);




	}
}
