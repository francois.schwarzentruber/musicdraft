class ImageManager {
    static getImage(filename) {
        let img = new Image();
        img.crossOrigin = "Anonymous";
        img.src = filename;
        return img;
    }

}

ImageManager.noire = ImageManager.getImage("images/blacknote.png");