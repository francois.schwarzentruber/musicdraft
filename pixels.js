function getPixel(context, x, y) {
  return context.getImageData(x, y, 1, 1).data[3];
}


function getImageData(context, x1, y1, width, height) {
  return { data: context.getImageData(x1, y1, width, height), width: width, x1: x1, y1: y1 };

}

function getImageDataPixel(imagedata, x, y) {
  return imagedata.data.data[((y - imagedata.y1) * imagedata.width + x - imagedata.x1) * 4 + 3];
}


function setImageDataPixel(imagedata, x, y, value) {
  imagedata.data.data[((y - imagedata.y1) * imagedata.width + x - imagedata.x1) * 4 + 3] = value;
}
