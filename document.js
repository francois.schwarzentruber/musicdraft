class Document {
	constructor(name) {
		this.name = name;
		this.pages = [];
		if (localStorage.getItem(this.name))
			this.load();
		else this.addPage();
		this.undoRedoStack = new UndoRedoStack();
	}

	addPage() { this.pages.push(new Page()); }


	update() {
		for (let page of this.pages) page.update();
	}

	getSerialize() {
		let obj = new Array();
		for (let i in this.pages)
			obj[i] = this.pages[i].getSerialize();
		return obj;
	}

	save() { localStorage.setItem(this.name, JSON.stringify(this.getSerialize())); }

	load() {
		console.log("loading " + this.name + "...")
		let obj = JSON.parse(localStorage.getItem(this.name));
		for (let i in obj) {
			console.log("  loading page " + i)
			this.addPage();

			let img = new Image();
			img.onload = () => {
				console.log("page" + i + " loaded");
				this.pages[i].getBackgroundContext().drawImage(img, 0, 0);
			}
			img.src = obj[i];

			//window.open(obj[i]);
		}
	}

	undo() { this.undoRedoStack.undo(); }
	redo() { this.undoRedoStack.redo(); }

	undoRedoScreenShot(page) {
		this.undoRedoStack.undoRedoScreenShot(page);
		if (page == this.pages[this.pages.length - 1])
			this.addPage();
		this.save();
	}

	downloadPDF() {
		const doc = new PDFDocument();
		let stream = doc.pipe(blobStream());

		let shouldAddNewPage = false;
		for (let page of this.pages) {
			if (shouldAddNewPage) doc.addPage();
			let pngData = page.getPNGData();
			doc.image(pngData, 40, 0, { width: 550, align: 'center', valign: 'top' });
			shouldAddNewPage = true;
		}
		doc.end();

		stream.on("finish", () => { this.displaySaveAsPDF(stream.toBlob("application/pdf")); });
	}

	displaySaveAsPDF(blob) {
		const a = document.createElement("a");
		document.body.appendChild(a);
		a.style = "display: none";

		var url = window.URL.createObjectURL(blob);
		a.href = url;
		a.download = 'test.pdf';
		a.click();
		window.URL.revokeObjectURL(url);
	}
}