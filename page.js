const noteradius = 6;
const scoreTop = noteradius * 16;
const staffmargin = noteradius * 20;
const lineMargin = 50;
const pageWidth = 1000;
const pageHeight = pageWidth * 297 / 210;

class Page {
    constructor() {
        this.page = $('<div class="Page"></div>');
        this.page.css({ width: pageWidth, height: pageHeight });
        this.canvasBackground = $('<canvas style="cursor: cross" class="canvasBackground" width="' + pageWidth + '" height="' + pageHeight + '"></canvas>');
        this.canvasForeground = $('<canvas style="cursor: cross" class="canvasForeground" width="' + pageWidth + '" height="' + pageHeight + '"></canvas>');

        this.page.append(this.canvasBackground);
        this.page.append(this.canvasForeground);

        $("#document").append(this.page);

        this.canvasForeground[0].page = this;
        this.canvasBackground[0].page = this;
        this.canvasForeground[0].addEventListener('mousedown', ev_canvas, false);
        this.canvasForeground[0].addEventListener('mousemove', ev_canvas, false);
        this.canvasForeground[0].addEventListener('mouseup', ev_canvas, false);

        this.canvasForeground[0].addEventListener('touchstart', ev_canvas, false);
        this.canvasForeground[0].addEventListener('touchmove', ev_canvas, false);
        this.canvasForeground[0].addEventListener('touchend', ev_canvas, false);

        this.drawLines();

    }



    getBackgroundContext() {
        return this.canvasBackground[0].getContext('2d');
    }

    getForegroundContext() {
        return this.canvasForeground[0].getContext('2d');
    }


    getBackgroundCanvas() {
        return this.canvasBackground[0];
    }

    getForegroundCanvas() {
        return this.canvasForeground[0];
    }

    getSerialize() {
        return this.canvasBackground[0].toDataURL();
    }

    getPNGData() {
        this.foregroundClear();
        this.getForegroundContext().drawImage(this.getBackgroundCanvas(), 0, 0);
        this.drawLines();
        return Canvas2Image.getPNGData(this.getForegroundCanvas(), this.getWidth(), this.getHeight());
    }

    drawLines() {
        let contextBackground = this.getForegroundContext();
        contextBackground.beginPath();
        contextBackground.strokeStyle = "black";
        //contextScreen.lineWidth   = 1;

        var nbStaffs = contextBackground.canvas.height / staffmargin;
        var width = contextBackground.canvas.width;
        for (let staff = 0; staff < nbStaffs; staff++) {
            for (let iline = 0; iline < 5; iline++) {
                var y = scoreTop + staff * staffmargin + iline * noteradius * 2;

                contextBackground.moveTo(lineMargin, y);
                contextBackground.lineTo(width - lineMargin, y);

            }
        }
        contextBackground.stroke();
    }


    foregroundClear() {
        this.getForegroundContext().clearRect(0, 0, this.getWidth(), this.getHeight());
    }




    foregroundDrawScore() {
        let contextScreen = this.getForegroundContext();
        if (tool.selection) {
            contextScreen.beginPath();
            contextScreen.strokeStyle = "red";
            contextScreen.rect(tool.selection.x1, tool.selection.y1, tool.selection.x2 - tool.selection.x1, tool.selection.y2 - tool.selection.y1);
            contextScreen.stroke();
        }
        this.drawLines();
    }



    getWidth() {
        return this.canvasForeground[0].width;
    }

    getHeight() {
        return this.canvasForeground[0].height;
    }

    update() {
        this.foregroundClear();
        this.foregroundDrawScore();
    }
}