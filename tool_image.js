// This painting tool works like a drawing pencil which tracks the mouse
// movements.
function tool_image(imgfilename, dx, dy, scale) {
  var tool = this;
  this.started = false;
  this.img = new Image();
  this.img.crossOrigin = "Anonymous";
  this.img.src = imgfilename;

  this.dx = dx;
  this.dy = dy;


  // This is called when you start holding down the mouse button.
  // This starts the pencil drawing.
  this.mousedown = function (ev, x, y) {
    y = getSurLigneOuEntre(y);
    ev.page.getBackgroundContext().drawImage(tool.img, x - tool.dx, y - tool.dy, tool.img.width * scale, tool.img.height * scale);
    doc.undoRedoScreenShot(ev.page);
    update();
  };


  // This function is called every time you move the mouse. Obviously, it only
  // draws if the tool.started state is set to true (when you are holding down
  // the mouse button).
  this.mousemove = function (ev, x, y) {
    update();
    y = getSurLigneOuEntre(y);
    ev.page.getForegroundContext().drawImage(tool.img, x - tool.dx, y - tool.dy, tool.img.width * scale, tool.img.height * scale);

  };

  // This is called when you release the mouse button.
  this.mouseup = function (ev) {

  };

  this.touchstart = this.mousedown;
  this.touchmove = this.mousemove;
  this.touchend = this.mouseup;


}
