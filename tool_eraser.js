// This painting tool works like a drawing pencil which tracks the mouse
// movements.
function tool_eraser() {
  $("#canvasScreen").css('cursor', 'none');
  var tool = this;

  this.started = false;
  this.lineWidth = 24;


  // This is called when you start holding down the mouse button.
  // This starts the pencil drawing.
  this.mousedown = function (ev, x, y) {
    contextErase(ev.page.getBackgroundContext(), x, y);
    tool.started = true;
  };

  // This function is called every time you move the mouse. Obviously, it only
  // draws if the tool.started state is set to true (when you are holding down
  // the mouse button).
  this.mousemove = function (ev, x, y) {
    if (tool.started) {
      contextErase(ev.page.getBackgroundContext(), x, y);
    }

    window.requestAnimationFrame(function () {
      update();
      contextScreen = ev.page.getForegroundContext();
      contextScreen.fillStyle = 'white';
      contextScreen.beginPath();
      contextScreen.arc(x, y, tool.lineWidth / 2, 0, Math.PI * 2, true);
      contextScreen.closePath();
      contextScreen.stroke();
      contextScreen.fill();
    });
  };

  // This is called when you release the mouse button.
  this.mouseup = function (ev, x, y) {
    if (tool.started) {
      tool.mousemove(ev, x, y);
      tool.started = false;
      doc.undoRedoScreenShot(ev.page);
    }
  };

  this.touchstart = this.mousedown;
  this.touchmove = this.mousemove;
  this.touchend = this.mouseup;




}






function contextErase(context, px, py) {
  var size = tool.lineWidth / 2;

  var imagedata = getImageData(context, px - size, py - size, 2 * size, 2 * size);

  for (var x = px - size; x <= px + size; x++)
    for (var y = py - size; y <= py + size; y++) {
      if ((x - px) * (x - px) + (y - py) * (y - py) < size * size)
        setImageDataPixel(imagedata, x, y, 0);

    }

  context.putImageData(imagedata.data, px - size, py - size);
}
