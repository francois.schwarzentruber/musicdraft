function keyBoardShortcutsLoad() {

    document.body.onkeydown = function (e) {
        var s = String.fromCharCode(e.keyCode);//+" --> "+e.keyCode);

        if (e.ctrlKey) {
            if (s == 'Z')
                doc.undo();
            else if (s == 'Y')
                doc.redo();
        }
        else {
            if (s == 'A')
                penButtonOnClick();
            else if (s == 'S')
                selectionButtonOnClick();
            else if (s == 'E')
                eraserButtonOnClick();
            else if (e.keyCode == 46) //delete keyCode
            {
                if (tool.selection != undefined) {
                    /*if there is a selection, then delete the selection!*/
                    tool.deleteSelection();
                    tool.selection = undefined;
                    update();
                }

            }


        }
    };
}

