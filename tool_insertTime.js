// This painting tool works like a drawing pencil which tracks the mouse 
// movements.
function tool_insertTime() {
  var tool = this;
  this.started = false;


  // This is called when you start holding down the mouse button.
  // This starts the pencil drawing.
  this.mousedown = function (ev) {
    tool.started = true;
    tool.x = ev.x;
  };

  // This function is called every time you move the mouse. Obviously, it only 
  // draws if the tool.started state is set to true (when you are holding down 
  // the mouse button).
  this.mousemove = function (ev) {
    if (tool.started) {
      y1 = (ev.y / systemHeight) * systemHeight;
      y2 = y1 + systemHeight;

      ev.page.getBackgroundContext().drawImage(ev.page.getBackgroundCanvas(), tool.x, y1, 32, systemHeight, ev.x, y1, 32, systemHeight);
      tool.x = ev.x;

      update();
      ev.page.getBackgroundContext().stroke();

    }
  };

  // This is called when you release the mouse button.
  this.mouseup = function (ev) {
    if (tool.started) {
      tool.mousemove(ev);
      tool.started = false;
      update();
    }
  };

  this.touchstart = this.mousedown;
  this.touchmove = this.mousemove;
  this.touchend = this.mouseup;


}
