



/*
distance({x: 0, y: 0}, {x: 1, y: 1})
*/
function distance(p1, p2)
{
	return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) +  (p1.y - p2.y) * (p1.y - p2.y));
}


/*
curveGetLength(clefdesolPoints)
*/
function curveGetLength(points)
{
        var length = 0;

        for(var i = 0; i < points.length-1; i++)
        {
            length += distance(points[i], points[i+1]);
        }

        return length;

}

/* this function considers the array points of Points as a curve and it gives
   the point at time t in [0, 1]*/
function curveGetPoint(points, t)
{
        var lengthtotal = curveGetLength(points);
        var d = lengthtotal * t;
        var  length = 0;
        var  lengthavant = 0;
        for(var i = 0; i < points.length-1; i++)
        {
            if((lengthavant <= d) & (d < length))
                return points[i];

            length += distance(points[i], points[i+1]);
            lengthavant = 0;
        }

        return points[points.length-1];
}




/* this function considers the array points of Points as a curve and it gives
   the angle at time t in [0, 1]*/
function curveGetAngle(points, t)
{
				var lengthtotal = curveGetLength(points);
				var d = lengthtotal * t;
				var  length = 0;
				var  lengthavant = 0;
        for(var i = 0; i < points.length-1; i++)
        {
            if((lengthavant <= d) & (d < length))
                return Math.atan2(points[i+1].y-points[i].y, points[i+1].x-points[i].x);

            length += distance(points[i], points[i+1]);
            lengthavant = 0;
        }

        return 0;

}





function curvesCompare(points1, points2)
{
        var note = 0;
        var nbstep = 35;
        for(var j = 0;  j < nbstep; j++)
        {
						var t = j/nbstep;
            note += Math.abs(curveGetAngle(points1, t) - curveGetAngle(points2, t));
        }
        return note;


}




function recognizeCurve(points)
{
		const seulSucces = 35;

	      var bestNote = 10000;
        var bestCurveDico = undefined;
        for(var curveDico of curveDictionnary)
        {
            var note = curvesCompare(curveDico.points, points);

            if(note < bestNote)
            {
                bestCurveDico = curveDico;
                bestNote = note;
            }
        }


        if(bestCurveDico == undefined)
            return false;


        if(bestNote < seulSucces)
        {
            bestCurveDico.action(points);
            return true;
        }
        else
            return false;




}












var curveDictionnary = new Array();


curveDictionnary.push({points: clefdefaPoints, action: function(points)
{
  var c = getBarycentre(points);

  var img = new Image();
  img.crossOrigin = "Anonymous";
  img.src = "images/FClef.svg";
  img.onload = function()
  {
      context.drawImage(img,c.x - img.width * 1.5/2, getYTopStaff(c.y) + img.height * 1.5 / 7, img.width * 1.5, img.height * 1.5);
      undoRedoScreenShot();
  };

}});








curveDictionnary.push({points: clefdesolPoints, action: function(points)
{
  var c = getBarycentre(points);

  var img = new Image();
  var imgscale = 2;
  img.crossOrigin = "Anonymous";
  img.src = "images/GClef.svg";
  img.onload = function()
  {
      context.drawImage(img,c.x - img.width * imgscale/2, getYTopStaff(c.y) - img.height * imgscale / 6, img.width * imgscale, img.height * imgscale);
      undoRedoScreenShot();
  };

}});
