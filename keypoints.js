function getKeyPoint(context, point) {
    var windowsize = 32;
    var epsilon = 7;
    var imagedata = getImageData(context, point.x - windowsize, point.y - windowsize, 2 * windowsize, 2 * windowsize);
    var points = new Array();

    var bestscore = 128;
    for (var x = point.x - epsilon; x <= point.x + epsilon; x++)
        for (var y = point.y - epsilon; y <= point.y + epsilon; y++) {
            delta = getImageDataPixel(imagedata, x + 1, y) +
                getImageDataPixel(imagedata, x - 1, y) +
                getImageDataPixel(imagedata, x, y + 1) +
                getImageDataPixel(imagedata, x, y - 1) - 4 * getImageDataPixel(imagedata, x, y);

            if (getImageDataPixel(imagedata, x, y))
                if (Math.abs(delta) > bestscore) {
                    //bestscore = 0;//Math.max(bestscore, Math.abs(delta));
                    points.push({ x: x, y: y, delta: bestscore });
                }

        }

    var bestpoints = new Array();

    for (var p of points) {
        if (p.delta == bestscore)
            bestpoints.push(p);

    }

    var keypoint;
    var bestdistance = 100000;

    for (var p of bestpoints) {
        var distance = (p.x - point.x) * (p.x - point.x) + (p.y - point.y) * (p.y - point.y);
        if (distance < bestdistance) {
            keypoint = p;
            bestdistance = distance;
        }


    }


    if (keypoint != undefined)
        keypoint.componentInformation = getComponentInformation(imagedata, point.x - windowsize, point.y - windowsize, point.x + windowsize, point.y + windowsize, keypoint);

    return keypoint;
}






function getComponentInformation(imagedata, x1, y1, x2, y2, keypoint) {
    var info = { minx: x2, miny: y2, maxx: x1, maxy: y1, density: 0 };
    var marked = new Array();

    var mark = function (p) {
        marked[p.x + p.y * 5000] = true;

    }


    var ismarked = function (p) {
        return marked[p.x + p.y * 5000];
    }

    var ispixel = function (x, y) {
        if (x < x1)
            return false;

        if (y < y1)
            return false;

        if (x > x2)
            return false;

        if (y > y2)
            return false;


        return getImageDataPixel(imagedata, x, y) != 0;

    }

    var visit = function (p) {
        if (ismarked(p))
            return;

        mark(p);

        if (info.minx > p.x)
            info.minx = p.x;

        if (info.miny > p.y)
            info.miny = p.y;

        if (info.maxx < p.x)
            info.maxx = p.x;

        if (info.maxy < p.y)
            info.maxy = p.y;

        for (var dx = -1; dx <= 1; dx++)
            for (var dy = -1; dy <= 1; dy++)
                if (ispixel(p.x + dx, p.y + dy))
                    visit({ x: p.x + dx, y: p.y + dy });

    }

    visit(keypoint);







    for (var x = info.minx; x <= info.maxx; x++)
        for (var y = info.miny; y <= info.maxy; y++)
            if (ispixel(x, y))
                info.density += 1;

    info.density = info.density / ((info.maxx - info.minx) * (info.maxy - info.miny));

    return info;
}
