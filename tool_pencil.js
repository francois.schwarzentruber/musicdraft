// This painting tool works like a drawing pencil which tracks the mouse
// movements.
const noteNoireTtheshold = 0.6;
const noteNoireTintervalinms = 100;
const noteNoireTdt = 0.2;



function tool_pencil() {
  $(".canvasForeground").css('cursor', 'crosshair');
  var points = new Array();
  var tool = this;
  this.started = false;
  this.noteNoireT = 0;
  this.noteNoireX = undefined;
  this.noteNoireY = undefined;

  // This is called when you start holding down the mouse button.
  // This starts the pencil drawing.
  this.mousedown = function (ev, x, y) {
    let ctx = ev.page.getForegroundContext();

    points = new Array();
    points.push({ x: x, y: y });
    tool.started = true;

    tool.noteNoireX = x;
    tool.noteNoireY = y;
    tool.noteNoireT = 0;
    console.log("mousedown");
    var keyPoint = getKeyPoint(ctx, { x: x, y: y });
    if (keyPoint != undefined) {
      if (Math.abs(tool.noteNoireY - keyPoint.y) < noteradius) {
        if (Math.abs(tool.noteNoireX - keyPoint.x) < noteradius) {
          if (tool.noteNoireX < keyPoint.x)
            tool.noteNoireX = keyPoint.x - noteradius;


          if (tool.noteNoireX > keyPoint.x)
            tool.noteNoireX = keyPoint.x + noteradius;
        }
      }
    }



    tool.points = points;

    stepForNoteNoire(ctx);

    ev.preventDefault();
  };

  // This function is called every time you move the mouse. Obviously, it only
  // draws if the tool.started state is set to true (when you are holding down
  // the mouse button).
  this.mousemove = function (ev, x, y) {

    if (tool.started) {

      points.push({ x: x, y: y });

      if (!isOKForNoteNoire(points)) {
        console.log("not a dot");

        if (tool.noteNoireT != 0) {
          tool.noteNoireT = 0;

          window.requestAnimationFrame(update);

        }

      }

      window.requestAnimationFrame(function () {
        let ctx = ev.page.getForegroundContext();
        ctx.strokeStyle = 'blue';
        drawPoints(ctx, points);
        stepForNoteNoireDraw(ctx);
      });






      ev.preventDefault();
    }
    else {

    }







  };

  // This is called when you release the mouse button.
  this.mouseup = function (ev, x, y) {
    if (tool.started) {
      tool.mousemove(ev, x, y);

      if (tool.noteNoireT > noteNoireTtheshold) {
        actionAddNoteNoire(ev.page.getBackgroundContext(), { x: tool.noteNoireX, y: getSurLigneOuEntre(tool.noteNoireY) });
      }
      else {
        recognize(ev.page.getBackgroundContext(), points);
      }
      doc.undoRedoScreenShot(ev.page);
      window.requestAnimationFrame(update);
      tool.started = false;
    }
  };

  this.touchstart = this.mousedown;
  this.touchmove = this.mousemove;
  this.touchend = this.mouseup;


}


function drawKeyPoint(context, point) {

  context.fillStyle = "rgba(0, 255, 255, 0.5)";
  context.lineWidth = 1;

  context.beginPath();
  context.arc(point.x, point.y, 8, 0, Math.PI * 2, true);
  context.fill();
  context.closePath();






}



function drawPoints(context, points) {
  if (points.length > 0) {
    context.beginPath();
    context.lineWidth = 1;

    context.moveTo(points[0].x, points[0].y);
    for (var i = 0; i < points.length; i++) {
      context.lineTo(points[i].x, points[i].y);
      context.stroke();
    }

    context.closePath();
  }


}






function stepForNoteNoireDraw(contextScreen) {
  if (tool.noteNoireT > 0) {
    var x = tool.noteNoireX;
    var y = tool.noteNoireY;


    y = getSurLigneOuEntre(y);

    var imgNoireRatio = ImageManager.noire.width / ImageManager.noire.height;
    var radius = noteradius * 0.8 * tool.noteNoireT;
    contextScreen.drawImage(ImageManager.noire, x - radius * imgNoireRatio, y - radius, 2 * radius * imgNoireRatio, 2 * radius);//, tool.img.width * 1.5, tool.img.height * 1.5);
  }


}



function isOKForNoteNoire(points) {
  var c = getBarycentre(points);
  const theshold = noteradius * 0.5;
  for (var p of points) {
    if (Math.abs(p.x - c.x) > theshold)
      return false;
    if (Math.abs(p.y - c.y) > theshold)
      return false;
  }

  c.y = getSurLigneOuEntre(c.y);
  return { x: c.x, y: c.y };


}

function stepForNoteNoire(context) {
  if (isDot(context, tool.points) && tool.started) {
    stepForNoteNoireDraw(context);

    if (tool.noteNoireT < 1)
      tool.noteNoireT += noteNoireTdt;


    setTimeout(() => stepForNoteNoire(context), noteNoireTintervalinms);

  }
}
