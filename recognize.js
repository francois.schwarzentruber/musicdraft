



function getSurLigneOuEntre(y) {
	return Math.round(y / noteradius) * noteradius;

}


function getSurLigne(y) {
	return Math.round(y / (2 * noteradius)) * 2 * noteradius;

}




function getYTopStaff(y) {
	var nbstaff = Math.round((y - scoreTop) / staffmargin);
	return scoreTop + nbstaff * staffmargin;

}

/*

-------------------- 0
                     1
-------------------- 2
                     3
-------------------- 4
                     5
-------------------- 6
                     7
-------------------- 8

*/
function getNumLigneOuInterligne(y) {
	var nbstaff = Math.round((y - scoreTop) / staffmargin);
	var ytop = scoreTop + staffmargin * nbstaff;

	return (y - ytop) / (noteradius);

}


function getBarycentre(points) {
	var cx = 0;
	var cy = 0;

	for (var p of points) {
		cx += p.x;
		cy += p.y;
	}

	cx = cx / points.length;
	cy = cy / points.length;

	return { x: cx, y: cy };
}





function isNoteNoire(context, points) {
	if (points.length < 10)
		return false;

	if (isCurveWithLittleChangeOfAngle(context, points))
		return false;

	var c = getBarycentre(points);

	c.y = getSurLigneOuEntre(c.y);

	for (var p of points) {
		if (Math.abs(p.x - c.x) > noteradius * 1.5) {
			console.log("pas une noire car trop large en X");
			return false;
		}

		if (Math.abs(p.y - c.y) > noteradius * 1.5) {
			console.log("pas une noire car trop large en Y");
			return false;
		}

	}

	var pnear = false;
	for (var p of points) {
		if (Math.abs(p.x - c.x) < noteradius * 0.3 && Math.abs(p.y - c.y) < noteradius * 0.3)
			pnear = true;
	}

	if (!pnear) {
		console.log("pas une noire car pas assez rempli");
		return false;
	}



	var pfar = false;
	for (var p of points) {
		if (Math.abs(p.x - c.x) > noteradius * 0.5)
			pfar = true;
	}


	if (!pfar) {
		console.log("pas une noire car pas assez de points loins en X");
		return false;
	}

	var pfar = false;
	for (var p of points) {
		if (Math.abs(p.y - c.y) > noteradius * 0.5)
			pfar = true;
	}


	if (!pfar) {
		console.log("pas une noire car pas assez de points loins en Y");
		return false;
	}


	for (var p of points) {
		if (p.y > c.y + noteradius)
			p.y = c.y + noteradius;

		if (p.y < c.y - noteradius)
			p.y = c.y - noteradius;
	}


	console.log("noire reconnue");

	return { x: c.x, y: c.y };

}




function isNoteRonde(context, points) {
	if (points.length < 10)
		return false;

	var c = getBarycentre(points);

	c.y = getSurLigneOuEntre(c.y);

	for (var p of points) {
		if (Math.abs(p.x - c.x) > noteradius * 1.5) {
			console.log("pas une ronde car trop large en X");
			return false;
		}

		if (Math.abs(p.y - c.y) > noteradius * 1.5) {
			console.log("pas une ronde car trop large en Y");
			return false;
		}

	}

	var pnear = false;
	for (var p of points) {
		if (Math.abs(p.x - c.x) < noteradius * 0.3 && Math.abs(p.y - c.y) < noteradius * 0.3)
			pnear = true;
	}

	if (pnear) {
		console.log("pas une ronde car des points trop près");
		return false;
	}



	var pfar = false;
	for (var p of points) {
		if (Math.abs(p.x - c.x) > noteradius * 0.5)
			pfar = true;
	}


	if (!pfar) {
		console.log("pas une ronde car pas assez de points loins en X");
		return false;
	}

	var pfar = false;
	for (var p of points) {
		if (Math.abs(p.y - c.y) > noteradius * 0.5)
			pfar = true;
	}


	if (!pfar) {
		console.log("pas une ronde car pas assez de points loins en Y");
		return false;
	}


	for (var p of points) {
		if (p.y > c.y + noteradius)
			p.y = c.y + noteradius;

		if (p.y < c.y - noteradius)
			p.y = c.y - noteradius;
	}




	return { x: c.x, y: c.y };

}







function isDot(context, points) {
	var c = getBarycentre(points);

	for (var p of points) {
		if (Math.abs(p.x - c.x) > 1)
			return false;
		if (Math.abs(p.y - c.y) > 1)
			return false;
	}

	c.y = getSurLigneOuEntre(c.y);
	return { x: c.x, y: c.y };


}





function isCurveWithLittleChangeOfAngle(context, points) {
	var angles = new Array();
	// for (var i = 0; i < points.length - 1; i++)
	// 	if ((points[i + 1].y != points[i].y) || (points[i + 1].x != points[i].x)) {
	// 		angles.push(Math.atan2(points[i + 1].y - points[i].y, points[i + 1].x - points[i].x));
	// 	}

	for (var i = 0; i < points.length - 1; i++)
		if ((points[i + 1].y != points[0].y) || (points[i + 1].x != points[0].x)) {
			angles.push(Math.atan2(points[i + 1].y - points[0].y, points[i + 1].x - points[0].x));
		}

	var anglemean = 0;
	for (var i = 0; i < angles.length; i++)
		anglemean += angles[i];

	anglemean = anglemean / (angles.length);

	var ecart = 0;
	for (var i = 0; i < angles.length; i++) {
		ecart += Math.abs(angles[i] - anglemean);
	}

	if (ecart > 1 * angles.length) {
		console.log("trait qui change beaucoup d'angles. Ecart : " + ecart);
		return false;

	}

	console.log("pas trop de changement d'angle. Ecart : " + ecart)
	return true;
}



function isBarreVerticale(context, points) {
	var c = getBarycentre(points);

	if (!isCurveWithLittleChangeOfAngle(context, points))
		return false;

	for (var p of points) {
		if (Math.abs(p.x - c.x) > 10)
			return false;
	}


	var y1 = points[0].y;
	var y2 = points[points.length - 1].y;

	var nbstaff1 = Math.round((y1 - scoreTop) / staffmargin);
	var goody1 = scoreTop + staffmargin * nbstaff1;

	if (Math.abs(goody1 - y1) > noteradius)
		return false;

	y1 = goody1;

	var nbstaff2 = Math.round((y2 - 4 * noteradius * 2 - scoreTop) / staffmargin);



	var goody2 = scoreTop + staffmargin * nbstaff2 + 4 * noteradius * 2;

	if (Math.abs(goody2 - y2) > noteradius)
		return false;

	y2 = goody2;

	return { x: c.x, y1: y1, y2: y2 };






}


function replaceByKeyPoint(context, point) {
	var keypoint = getKeyPoint(context, point);

	if (keypoint)
		return keypoint;

	return point;
}






function isKeyPointValidForTraitVertical(context, keypoint) {
	if (keypoint.componentInformation.density <= 0.5) //not dense, must be big
	{
		if (keypoint.componentInformation.maxx - keypoint.componentInformation.minx < 4 * noteradius &&
			keypoint.componentInformation.maxy - keypoint.componentInformation.miny < 4 * noteradius)
			return false;
	}
	if (keypoint.componentInformation.density > 0.5) //dense, must be a note
	{
		if (keypoint.componentInformation.maxx - keypoint.componentInformation.minx < 0.5 * noteradius ||
			keypoint.componentInformation.maxy - keypoint.componentInformation.miny < 0.5 * noteradius)
			return false;
	}

	return true;

}

function isTraitVertical(context, points) {
	var c = getBarycentre(points);

	if (!isCurveWithLittleChangeOfAngle(context, points))
		return false;

	for (var p of points) {
		if (Math.abs(p.x - c.x) > noteradius)
			return false;
	}

	if (Math.abs(points[0].y - points[points.length - 1].y) < 6 * noteradius)
		return false;

	var keypoint0 = getKeyPoint(context, points[0]);

	if (keypoint0 != undefined) {
		if (isKeyPointValidForTraitVertical(context, keypoint0))
			points[0] = keypoint0;
	}

	var keypoint1 = getKeyPoint(context, points[points.length - 1]);

	if (keypoint1 != undefined) {
		if (isKeyPointValidForTraitVertical(context, keypoint1))
			points[points.length - 1] = keypoint1;
	}
	console.log("trait vertical");

	return { x: points[0].x, y1: points[0].y, y2: points[points.length - 1].y };

}






function isPetitTraitHorizontal(context, points) {
	var c = getBarycentre(points);

	if ((-1 <= getNumLigneOuInterligne(c.y)) && (getNumLigneOuInterligne(c.y) <= 9)) return false;

	if (!isCurveWithLittleChangeOfAngle(context, points)) {
		console.log("pas petit trait horizontal car trop de changement d'angles");
		return false;
	}

	for (var p of points)
		if (Math.abs(p.y - c.y) > noteradius) {
			console.log("pas petit trait horizontal car trop loin en y");
			return false;
		}


	let first = points[0];
	let last =  points[points.length - 1];

	if(Math.abs(Math.atan2(first.y - last.y, first.x - last.x)) < 0.03) {
		console.log("pas petit trait horizontal car pas horizontal")
		return false;
	}

	var x1 = points[0].x;
	var x2 = points[points.length - 1].x;

	if (Math.abs(x1 - x2) > 5 * noteradius) {
		console.log("pas petit trait horizontal car trop long");
		return false;
	}

	c.y = getSurLigne(c.y);

	console.log("petit trait horizontal");
	return { x1: x1, x2: x2, y: c.y };
}









function isKeyPointValidForTraitCrocheDroit(context, keypoint) {
	//must be big
	return ((keypoint.componentInformation.maxx - keypoint.componentInformation.minx > 4 * noteradius) ||
		(keypoint.componentInformation.maxy - keypoint.componentInformation.miny > 4 * noteradius));
}



function isTraitCrocheDroit(context, points) {
	var keypoint0 = getKeyPoint(context, points[0]);

	if (keypoint0 == undefined)
		return false;

	if (!isKeyPointValidForTraitCrocheDroit(context, keypoint0))
		return false;

	//si le trait "veut" partir d'un trait de croche déjà présent mais pris par en bas => non.
	if (!(points[0].y > keypoint0.y + noteradius / 2))
		points[0] = keypoint0;

	var keypoint1 = getKeyPoint(context, points[points.length - 1]);

	if (keypoint1 != undefined) {
		if (isKeyPointValidForTraitCrocheDroit(context, keypoint1))
			points[points.length - 1] = keypoint1;
	}


	return true;

}





function actionAddNoteNoire(context, r) {
	var radius = noteradius * 0.8;
	let imgNoireRatio = ImageManager.noire.width / ImageManager.noire.height;
	console.log("image noire dessinée");
	context.drawImage(ImageManager.noire, r.x - radius * imgNoireRatio, r.y - radius, 2 * radius * imgNoireRatio, 2 * radius);
}


function actionAddNoteRonde(context, r) {
	var imgRonde = new Image();
	imgRonde.crossOrigin = "Anonymous";
	imgRonde.src = "images/wholenote.png";
	var imgRondeRatio = imgRonde.width / imgRonde.height;
	var radius = noteradius * 0.8;
	context.drawImage(imgRonde, r.x - radius * imgRondeRatio, r.y - radius, 2 * radius * imgRondeRatio, 2 * radius);


}

function recognize(context, points) {
	context.strokeStyle = "#000000";
	context.lineWidth = 2;
	var r;

	r = isNoteNoire(context, points);
	if (r) {
		actionAddNoteNoire(context, r);
		return;
	}

	r = isNoteRonde(context, points);
	if (r) {
		actionAddNoteRonde(context, r);
		return;
	}


	r = isDot(context, points);
	if (r) {
		context.fillStyle = "#000000";
		context.beginPath();
		context.arc(r.x, r.y, 2, 0, Math.PI * 2, true);
		context.closePath();
		context.fill();

		return;
	}


	r = isBarreVerticale(context, points);
	if (r) {
		context.beginPath();
		context.moveTo(r.x, r.y1);
		context.lineTo(r.x, r.y2);
		context.closePath();
		context.stroke();

		return;
	}

	r = isTraitVertical(context, points);
	if (r) {
		context.beginPath();
		context.moveTo(r.x, r.y1);
		context.lineTo(r.x, r.y2);
		context.closePath();
		context.stroke();

		return;
	}




	r = isPetitTraitHorizontal(context, points);

	if (r) {
		context.beginPath();
		context.moveTo(r.x1, r.y);
		context.lineTo(r.x2, r.y);
		context.closePath();
		context.stroke();

		return;

	}

	r = isTraitCrocheDroit(context, points);

	if (r) {

		for (var i = 0; i < 5; i++) {
			drawPoints(context, points);
			for (var p of points) {
				p.y += 1;
			}

		}

		return;
	}








	if (recognizeCurve(context, points)) {
		return;
	}
	for (var i = 0; i < 3; i++) {
		drawPoints(context, points);
		for (var p of points) {
			p.y += 1;
		}

	}

}
