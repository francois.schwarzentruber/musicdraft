let tool; //current tool
let doc = undefined; //current document

function load() {
	let name = new URLSearchParams(location.search).get("name");
	doc = new Document(name);
	keyBoardShortcutsLoad();
	tool = new tool_pencil();

	$('#canvasElem').bind('contextmenu', e => false);
	//paletteCreate();
}


window.onload = load;


function tool_quit() {
	if (tool.quit != null) tool.quit();
	update();
}



function penButtonOnClick() {
	tool_quit();
	tool = new tool_pencil();
}


function eraserButtonOnClick() {
	tool_quit();
	tool = new tool_eraser();
}




function insertTimeButtonOnClick(imgfilename, dx, dy) {
	tool_quit();
	tool = new tool_insertTime();
}

function selectionButtonOnClick() {
	tool_quit();
	tool = new tool_selection();
}


function getMousePos(canvas, clientX, clientY) {
	var rect = canvas.getBoundingClientRect();
	return {
		x: clientX - rect.left,
		y: clientY - rect.top
	};
}


// The general-purpose event handler. This function just determines the mouse
// position relative to the canvas element.
function ev_canvas(ev) {
	canvasScreen = ev.target;
	ev.preventDefault();

	var coord = getMousePos(canvasScreen, ev.clientX, ev.clientY);


	var touches = ev.changedTouches;

	if (touches) {
		for (var i = 0; i < touches.length; i++) {
			coord = getMousePos(canvasScreen, touches[i].pageX, touches[i].pageY);
		}

	}

	ev.x = coord.x;
	ev.y = coord.y;

	ev.page = ev.target.page;

	// Call the event handler of the tool.
	var func = tool[ev.type];
	if (func) {
		func(ev, coord.x, coord.y);
	}
}



function update(page) {
	doc.update(page);
}