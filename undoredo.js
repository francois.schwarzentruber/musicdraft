class UndoRedoStackElement {
    constructor(page, urlImage) {
        this.page = page;
        this.urlImage = urlImage;
    }

    getPage() { return this.page; }
    getUrlImage() { return this.urlImage; }

}



class UndoRedoStack {
    constructor() {
        this.cPushArray = new Array();
        this.cStep = -1;
    }

    undoRedoScreenShot(page) {
        console.log("undoRedoScreenShot");
        this.cStep++;
        if (this.cStep < this.cPushArray.length) { this.cPushArray.length = this.cStep; }
        this.cPushArray.push(new UndoRedoStackElement(page, page.canvasBackground[0].toDataURL()));
    }

    undo() {
        console.log("undo");
        if (this.cStep >= 0) {
            var canvasPic = new Image();
            let element = this.cPushArray[this.cStep];
            canvasPic.src = element.getUrlImage();
            canvasPic.onload = function () {
                element.page.getBackgroundContext().clearRect(0, 0, element.page.getWidth(), element.page.getHeight());
                element.page.getBackgroundContext().drawImage(canvasPic, 0, 0);
                update();
            };
            this.cStep--;
        }

    }



    redo() {
        if (this.cStep < this.cPushArray.length - 1) {
            this.cStep++;
            var canvasPic = new Image();
            let element = this.cPushArray[this.cStep];
            canvasPic.src = element.getUrlImage();
            canvasPic.onload = function () {
                element.page.getBackgroundContext().clearRect(0, 0, element.page.getWidth(), element.page.getHeight());
                element.page.getBackgroundContext().drawImage(canvasPic, 0, 0);
                update();
            }
        }
        update();
    }


}







