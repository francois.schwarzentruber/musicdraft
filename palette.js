var currentSection = null;


function paletteCreate() {
	paletteNewSection('Clefs');
	paletteAddButtonForAddingImageSelection();
	paletteAddImageButton('images/GClef.svg', 0, 0, 1.5);
	paletteAddImageButton('images/CClef.svg', 0, 0, 1.5);
	paletteAddImageButton('images/FClef.svg', 0, -2, 1.5);

	paletteNewSection('Notes');
	paletteAddImageButton('images/Quarter_note.gif', 12, 49, 1.5);
	paletteAddImageButton('images/Half_note.gif', 12, 49, 1.5);
	paletteAddImageButton('images/wholenote.png', 12, 49, 1.5);

	paletteNewSection('Rests');
	paletteAddImageButton('images/Whole_rest.svg', 0, 5, 1.5);
	paletteAddImageButton('images/Breve_rest.svg', 0, 5, 1.5);
	paletteAddImageButton('images/Quarter_rest.svg', 0, 0, 1);
	paletteAddImageButton('images/Eighth_rest.svg', 0, 0, 1);
	paletteAddImageButton('images/16th_rest.svg', 0, 0, 1);
	paletteAddImageButton('images/32nd_rest.svg', 0, 0, 1);
	paletteAddImageButton('images/64th_rest.svg', 0, 0, 1);
	paletteAddImageButton('images/128th_rest.svg', 0, 0, 1);

	paletteNewSection('My pattern');

}



function paletteNewSection(name) {
	//  var currentSectionTitle = document.createElement("h3");
	//	currentSectionTitle.innerHTML = '<h3><a href="#">' + name + '</a></h3';
	//	document.getElementById("accordion").appendChild(currentSectionTitle);
	currentSection = document.createElement("div");
	//	currentSection.style.width = "200px";
	document.getElementById("accordion").appendChild(currentSection);
}


function imageButtonOnClick(imgfilename, dx, dy, scale) {
	tool_quit();
	tool = new tool_image(imgfilename, dx, dy, scale);
}



function paletteAddImageButton(imgfilename, dx, dy, scale) {
	var button = $('<button></button>').click(function () { imageButtonOnClick(imgfilename, dx, dy, scale); });
	button.addClass('buttonPalette');

	var img = $('<img/>');
	img.attr('src', imgfilename);
	img.attr('width', '80%');
	button.append(img);
	$(currentSection).append(button);
	//  currentSection.innerHTML += "<button OnClick=\"imageButtonOnClick(" + imgfilename + "', " + dx + ", " + dy + ", " + scale + ")\"><img width=80%  src='" + imgfilename + "'></button>";
}


function paletteAddButtonForAddingImageSelection() {
	currentSection.innerHTML += "<button title='Add a selection to the palette' onclick='paletteAddImageSelection()'><img height=24px src='add.png'/></button>";

}







// The crop function
var crop = function (canvas, offsetX, offsetY, width, height, callback) {
	// create an in-memory canvas
	var buffer = document.createElement('canvas');
	var b_ctx = buffer.getContext('2d');
	// set its width/height to the required ones
	buffer.width = width;
	buffer.height = height;
	// draw the main canvas on our buffer one
	// drawImage(source, source_X, source_Y, source_Width, source_Height,
	//  dest_X, dest_Y, dest_Width, dest_Height)
	b_ctx.drawImage(canvas, offsetX, offsetY, width, height,
		0, 0, buffer.width, buffer.height);
	// now call the callback with the dataURL of our buffer canvas
	callback(buffer.toDataURL());
};



function paletteAddImageSelection() {
	if (tool.selection) {
		var width = tool.selection.x2 - tool.selection.x1;
		var height = tool.selection.y2 - tool.selection.y1;
		crop(canvas, tool.selection.x1, tool.selection.y1, width, height, function (dataURL) {
			paletteAddImageButton(dataURL, width / 2, getSurLigneOuEntre(height / 2), 1);

		});

	}
}
